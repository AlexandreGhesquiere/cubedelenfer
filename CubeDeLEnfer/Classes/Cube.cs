﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubeDeLEnfer.Classes
{
	 class Cube
	{
		private string[,,] cube = new String[3, 3, 3]
		{
			{
				{"<empty>","<empty>","<empty>"},
				{"<empty>","<empty>","<empty>"},
				{"<empty>","<empty>","<empty>"}
			},
			{
				{"<empty>","<empty>","<empty>"},
				{"<empty>","<empty>","<empty>"},
				{"<empty>","<empty>","<empty>"}
			},
			{
				{"<empty>","<empty>","<empty>"},
				{"<empty>","<empty>","<empty>"},
				{"<empty>","<empty>","<empty>"}
			}
		};

		public int Count { get; private set; } = 0;

		public void SetXYZ(int i, out int x, out int y, out int z)
		{
			x = i % 3;
			y = (i / 3) % 3;
			z = i / 9;
		}

		public void SetXYZ(Axes axe, int deep, int i, int j, out int x, out int y, out int z)
		{
			switch (axe)
			{
				case Axes.Z:
					x = i;
					y = j;
					z = deep;
					break;
				case Axes.X:
					x = deep;
					y = j;
					z = i;
					break;
				case Axes.Y:
					x = i;
					y = deep;
					z = j;
					break;
				default:
					x = 0;
					y = 0;
					z = 0;
					break;

			}
		}

		public void Write (string message)
		{
			Console.WriteLine(message);	
		}

		public string Read()
		{
			String messageCrypte = "null";
			return messageCrypte;
		}

		public string [,] GetSlice(Axes axe, int deep)
		{
			//Récupérer la tranche
			

			return null;
		}

		public void SetSlice (Axes axe, int deep, string [,] slice)
		{

		}

		public string [,] Rotate (string [,] slice)
		{
			return null;
		}

		public string [,] Rotate (string [,] slice, int count)
		{
			
			return null;
		}

		public void Rotate (Axes axe, int deep, int count)
		{

		}

		
		

	}
}
