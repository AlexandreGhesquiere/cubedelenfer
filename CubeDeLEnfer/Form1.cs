﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CubeDeLEnfer.Classes;
namespace CubeDeLEnfer
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void EntreeBox_TextChanged(object sender, EventArgs e)
		{
			string messageEntrant = EntreeBox.Text;
		}

		private void SortieBox_TextChanged(object sender, EventArgs e)
		{
			string messageSortant = SortieBox.Text;

			
		}

		private void Executer_Click(object sender, EventArgs e)
		{
			Cube cube = new Cube();
			string messageEntrant = EntreeBox.Text;

			if (messageEntrant.Length > 27)
			{
				MessageBox.Show("Votre message depasse 27 caractères, il doit en comporter moins");
			}
			else
			{
				cube.Write(messageEntrant);
				SortieBox.Text = cube.Read();
			}
		}
	}
}
