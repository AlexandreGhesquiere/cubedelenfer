﻿namespace CubeDeLEnfer
{
	partial class Form1
	{
		/// <summary>
		/// Variable nécessaire au concepteur.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Nettoyage des ressources utilisées.
		/// </summary>
		/// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Code généré par le Concepteur Windows Form

		/// <summary>
		/// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette méthode avec l'éditeur de code.
		/// </summary>
		private void InitializeComponent()
		{
			this.TitreMessage = new System.Windows.Forms.Label();
			this.EntreeBox = new System.Windows.Forms.TextBox();
			this.SortieMessage = new System.Windows.Forms.Label();
			this.SortieBox = new System.Windows.Forms.TextBox();
			this.Executer = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// TitreMessage
			// 
			this.TitreMessage.AutoSize = true;
			this.TitreMessage.Location = new System.Drawing.Point(12, 9);
			this.TitreMessage.Name = "TitreMessage";
			this.TitreMessage.Size = new System.Drawing.Size(91, 13);
			this.TitreMessage.TabIndex = 0;
			this.TitreMessage.Text = "Entrer le message";
			// 
			// EntreeBox
			// 
			this.EntreeBox.Location = new System.Drawing.Point(15, 25);
			this.EntreeBox.Name = "EntreeBox";
			this.EntreeBox.Size = new System.Drawing.Size(100, 20);
			this.EntreeBox.TabIndex = 1;
			this.EntreeBox.TextChanged += new System.EventHandler(this.EntreeBox_TextChanged);
			// 
			// SortieMessage
			// 
			this.SortieMessage.AutoSize = true;
			this.SortieMessage.Location = new System.Drawing.Point(224, 9);
			this.SortieMessage.Name = "SortieMessage";
			this.SortieMessage.Size = new System.Drawing.Size(94, 13);
			this.SortieMessage.TabIndex = 2;
			this.SortieMessage.Text = "Sortie du message";
			// 
			// SortieBox
			// 
			this.SortieBox.Location = new System.Drawing.Point(227, 25);
			this.SortieBox.Name = "SortieBox";
			this.SortieBox.Size = new System.Drawing.Size(100, 20);
			this.SortieBox.TabIndex = 3;
			this.SortieBox.TextChanged += new System.EventHandler(this.SortieBox_TextChanged);
			// 
			// Executer
			// 
			this.Executer.Location = new System.Drawing.Point(15, 79);
			this.Executer.Name = "Executer";
			this.Executer.Size = new System.Drawing.Size(75, 23);
			this.Executer.TabIndex = 4;
			this.Executer.Text = "Executer";
			this.Executer.UseVisualStyleBackColor = true;
			this.Executer.Click += new System.EventHandler(this.Executer_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.Executer);
			this.Controls.Add(this.SortieBox);
			this.Controls.Add(this.SortieMessage);
			this.Controls.Add(this.EntreeBox);
			this.Controls.Add(this.TitreMessage);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label TitreMessage;
		private System.Windows.Forms.TextBox EntreeBox;
		private System.Windows.Forms.Label SortieMessage;
		private System.Windows.Forms.TextBox SortieBox;
		private System.Windows.Forms.Button Executer;
	}
}

